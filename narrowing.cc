#include <iostream>

int main(int argc, char *argv[])
{
    // C++ 11 list-initialization 列表初始化，严格，大值无法转换
    const int code = 65;
    int x = 65;
    char c1 {1234};
    char c2 = {65};
    char c3 {code};
    char c4 = {x};
    x = 1234;
    char c5 = x;
    return 0;
}
